# Kotlin
### Android Developement
---
## Android Studio installieren
[https://developer.android.com/studio/index.html?](https://developer.android.com/studio/index.html?)
+++
### Android Studio inkl. Emulator installieren
![Video](https://www.youtube.com/embed/C9vo6foX5pU)
---
## Ein Projekt anlegen
+++
### Projekt Informationen
<img src="http://drive.google.com/uc?export=view&id=1tbAwWAdRttyOWQ9cMcHIPMYu81ml2h3s" width="70%"/>
+++
### API Level
<img src="http://drive.google.com/uc?export=view&id=1lOQBrLdzBunbthpynEO6yQp46g-f_9le" width="70%"/>
+++
### Activity Template
<img src="http://drive.google.com/uc?export=view&id=1idxySKHejRblVpCqJkAj1WImfKd67qqb" width="70%"/>
+++
### Activity Namen
<img src="http://drive.google.com/uc?export=view&id=14q1G9cyrG9Apbq7FLYYoIqV_FtD8bH_A" width="70%"/>
---
## Tour durch Android Studio
+++
### Sicht beim entwickeln
<img src="http://drive.google.com/uc?export=view&id=1uDUg_-FZr_xrWwwmlFa2_h51VEvdosGA" width="70%"/>
+++
### XML Layout 
<img src="http://drive.google.com/uc?export=view&id=1IRgpT1bdQQj-4drVHfSCL5QWxWTAPWOF" width="70%"/>
+++
### WYSIWYG Editor
<img src="http://drive.google.com/uc?export=view&id=1YtYmlvCKYx4LRsPOkRpaMSkYAtGxDN7p" width="70%"/>
---
## Die erste Android App
+++
### Swiss Airport Finder
<img src="http://drive.google.com/uc?export=view&id=1fVIdN7ws3zUUd0FvarqUp4fVnt8WFnF4" width="30%"/>
+++
### Widgets
<img src="http://drive.google.com/uc?export=view&id=1-sfeKDw59QlG91CCGpzWIC3VY11NI0CP" width="30%"/>
+++
### Constraint Layout
<img src="http://drive.google.com/uc?export=view&id=1TofEhubaQoXQ9KXjifsVnuCj3CUXox45" width="30%"/>
+++
### Id's
<img src="http://drive.google.com/uc?export=view&id=1XTIUc2mxBiI-0ueN510OHMV1zLmWcn1V" width="30%"/>
+++
### Model
```kotlin
package ch.teko.olten.android.swissairportfinder.model

object SwissAirports {

    private val airportMap = mapOf(
            "Zürich" to "ZRH",
            "Samedan" to "SMV",
            "St. Gallen" to "ACH",
            "Sion" to "SIR",
            "Lugano" to "LUG",
            "Genf" to "GVA",
            "Buochs" to "BXO",
            "Bern" to "BRN",
            "Basel" to "BSL"
    )

    fun getAirportName(ort:String) : String {
        return airportMap.getOrDefault(ort, "Sorry, in dieser Ortschaft kenne ich keinen Flughafen!")
    }

}

```
+++
### Activity
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        searchBtn.setOnClickListener{
            val airportName = SwissAirports.getAirportName(locationTxt.text.toString())
            airportTxt.setText(airportName)
        }
    }
}

```
+++
### GIT
[https://gitlab.com/gfelline/airport-finder](https://gitlab.com/gfelline/airport-finder)
---
## Aufgabe
+++
### Ausredengenerator

Eine Ausrede besteht aus einem Satz mit vier Zufallswörter:

> Es liegt an `[Zufallswort 1]` `[Zufallswort 2]` im `[Zufallswort 3]` `[Zufallswort 4]`.

+++
### Benötigte Listen
```kotlin
val woerter_1 = listOf("Harmonische", "Solaren","Termalen","Linearen","Heisenbergschen","Kollidierten","Magnetischen","Konstanten","Subharmonischen")
val woerter_2 = listOf("Störungen","Interferenzen", "Kollisionen","Interups", "Determinierungen","Kompilierungen")
val woerter_3 = listOf("RAM", "CD", "LAN","WAN","CPU","HDD", "IOT")
val woerter_4 = listOf("Bus", "Controller","Stack","Array","Communications Interfaces","Treiber")

```
+++
### Bild Resource
<img src="http://drive.google.com/uc?export=view&id=1SnwrmqXVCA5gqZs7waJjmX9c8ZBZZGxZ" width="100%"/>
+++
### Demo
![Video](https://www.youtube.com/embed/Jevkzd5-jMg)
---
## Layouts
+++
[https://developer.android.com/guide/topics/ui/declaring-layout.html](https://developer.android.com/guide/topics/ui/declaring-layout.html)
---
## Constraint Layout
+++
[https://developer.android.com/training/constraint-layout/index.html](https://developer.android.com/training/constraint-layout/index.html)
+++
## Beispiel
<img src="http://drive.google.com/uc?export=view&id=1D2wJhF7FPfiy0Xz1uwQlKA7LTOzkgSXE" width="80%"/>
+++
### GIT
[https://bitbucket.org/androidkotlin/constraint-layout/src/master/](https://bitbucket.org/androidkotlin/constraint-layout/src/master/)
---
## Alternative Layouts erstellen
+++
### Z.Bsp.: Landscape Modus
<img src="http://drive.google.com/uc?export=view&id=1VM7NwsE6qs-ucrWOqs9eexFLgwci5xvg" width="65%"/>
+++
### Alternatives Layout erstellt
<img src="http://drive.google.com/uc?export=view&id=1RGa3DD9kFi_MRKdVdIgA_wXxkN1_3qm6" width="80%"/>
+++
### Alterantives Layout redesignen
<img src="http://drive.google.com/uc?export=view&id=1BVR_SzAKn6fjNuuG0g0JVVTOzbKVd-2J" width="80%"/>
+++
### GIT
[https://bitbucket.org/androidkotlin/constraint-layout/src/landscape/](https://bitbucket.org/androidkotlin/constraint-layout/src/landscape/)
---
## Elemente programmatisch der View hinzufügen
+++
### Ziel
<img src="http://drive.google.com/uc?export=view&id=19pqtA3FgGHBhbewA9SiFPusm9qoBQP0z" width="65%"/>
+++
![Video](https://www.youtube.com/embed/MZt9GIZ2X4Q)
+++
### Aufgabe
Let's code :)
+++
### Lösung
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val antworten = RadioGroup(this)
        antworten.addView(buildAntwortRadioButton("Gut"))
        antworten.addView(buildAntwortRadioButton("Mittel"))
        antworten.addView(buildAntwortRadioButton("Schlecht"))

        antwortLayout.addView(antworten)

        val klickButton = Button(this)
        klickButton.text = "Klick!"

        antwortLayout.addView(klickButton)

        klickButton.setOnClickListener {
            val ausgewaehlteAntwortId = antworten.checkedRadioButtonId
            val ausgewaehlteAntwortRadioButton = findViewById<RadioButton>(ausgewaehlteAntwortId)

            Toast.makeText(this, "Klick: ${ausgewaehlteAntwortRadioButton.tag}", Toast.LENGTH_LONG).show()
        }
    }

    private fun buildAntwortRadioButton(antwort: String): RadioButton {
        val antwortButton = RadioButton(this)
        antwortButton.id = View.generateViewId()
        antwortButton.text = antwort
        antwortButton.tag = "$antwort Button"
        return antwortButton
    }
}
```
+++
### GIT
[https://bitbucket.org/androidkotlin/elemente-programmatisch-hinzugefuegt/src/master/](https://bitbucket.org/androidkotlin/elemente-programmatisch-hinzugefuegt/src/master/)
---
## Activities
+++
### Konzept
<span style="font-size: 0.7em;">
Das mobile App-Erlebnis unterscheidet sich von seinem Desktop-Pendant dadurch, dass die Interaktion eines Benutzers mit der App nicht immer an der gleichen Stelle beginnt. Stattdessen beginnt die Nutzerreise oft nicht-deterministisch. Wenn Sie beispielsweise eine E-Mail-Anwendung von Ihrem Startbildschirm aus öffnen, wird möglicherweise eine Liste mit E-Mails angezeigt. Wenn Sie dagegen eine Social-Media-Anwendung verwenden, die dann Ihre E-Mail-Anwendung startet, können Sie direkt auf den Bildschirm der E-Mail-Anwendung gehen, um eine E-Mail zu verfassen.
</span>
+++
### Activity Klasse
<span style="font-size: 0.7em;">
Die Aktivitätsklasse soll dieses Paradigma erleichtern. Wenn eine App eine andere aufruft, ruft die aufrufende App eine Aktivität in der anderen App auf und nicht die App als atomares Ganzes. Auf diese Weise dient die Aktivität als Einstiegspunkt für die Interaktion einer App mit dem Benutzer.
</span>
+++
### Doku
[https://developer.android.com/guide/components/
activities/intro-activities.html](https://developer.android.com/guide/components/activities/intro-activities.html)
+++
### Active und Visible Lifetime einer Acitivy
![Video](https://www.youtube.com/embed/85MppyLJHz0)
+++
### Visibile Lifetime einer Activity
![Video](https://www.youtube.com/embed/88rJq9HyGLI)
+++
### Lebenszyklus einer Acitivy
<img src="http://drive.google.com/uc?export=view&id=1jKqy7xtENRSPsVY3t_8AjTZhtaeUzSlv" width="40%" height="40%"/>
+++
### Aufgabe
![Video](https://www.youtube.com/embed/rYHF4x52JtY)
+++
### Doku
[https://developer.android.com/guide/components/
activities/activity-lifecycle.html](https://developer.android.com/guide/components/activities/activity-lifecycle.html)
---
## Basic Activity
+++
### Basic Activity erstellen
<img src="http://drive.google.com/uc?export=view&id=1b7fqS9FDLX4m4Sp_4xx9tNAEAmlWBeue" width="70%"/>
+++
### Zusammengesetzte View
<img src="http://drive.google.com/uc?export=view&id=1C1BeyCAwNfbIfzH9zL6y5F_FCBaP-Lqi" width="80%"/>
+++
### Besteht aus mehreren XML's
<img src="http://drive.google.com/uc?export=view&id=10SNiZA7iZ6P7Yjvydd_fGAmKIIFSIha2" width="70%"/>
+++
### Activity Implementierung
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setC5ontentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
```
@[5](activity_main XML laden (inflate))
@[6](toolbar setzen)
@[8-11](Snakbar soll erscheinen wenn auf das Floating Action Button geklickt wird)
@[14-17](Das menu_main XML als Option Menu laden)
+++
### Menu
<img src="http://drive.google.com/uc?export=view&id=1wHCJarod1wepzrh8KLy782UFO2sDqXQT" width="70%"/>
+++
### Demo
![Video](https://www.youtube.com/embed/L8Z8VJyLxn8)
+++
### GIT
[https://bitbucket.org/androidkotlin/basic-activity/src/master/](https://bitbucket.org/
androidkotlin/basic-activity/src/master/)
---
## Verschiedene Sprachen untersützen
+++
### Deutsche Texte fix hinterlegt
<img src="http://drive.google.com/uc?export=view&id=1wbwo0x9YmCYhq69QQZKpNA1DTZ5M3pvW" width="70%"/>
+++
### String Resourcen in verschiedenen Sprachen hinzufügen
![Video](https://www.youtube.com/embed/HQ27OtJ2n5Y)
+++
### GIT
[https://bitbucket.org/androidkotlin/internationalisierung/src/master/](https://bitbucket.org/androidkotlin/internationalisierung/src/master/)
---
## Masseinheit DP
+++
### Doku
[](https://material.io/design/layout/understanding-layout.html#pixel-density)
[](http://developer.android.com/guide/topics/resources/more-resources.html#Dimension)]
---
## Kontaktdaten App
### ListView
+++
### ListView
<img src="http://drive.google.com/uc?export=view&id=1LZ7q56-uPqudSSKGDN0kwb2e4tXGqYUk" width="70%"/>
+++
### ArrayAdapter
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val kontakte = ArrayList<String>()

        kontakte.add("Tom")
        kontakte.add("Alex")
        kontakte.add("Susi")
        kontakte.add("Sabrina")
        kontakte.add("Mario")
        kontakte.add("Luigi")
        kontakte.add("Susanne")
        kontakte.add("Tobi")
        kontakte.add("Gianni")
        kontakte.add("Miriam")
        kontakte.add("Luana")
        kontakte.add("Max")

        val kontakteAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kontakte)
        kontaktListe.adapter = kontakteAdapter

        kontaktListe.setOnItemClickListener { parent, view, position, id ->
            view as TextView
            Toast.makeText(this, "Geklickt wurde: ${view.text}", Toast.LENGTH_SHORT).show()
        }
    }
}
```
+++
### Model
```kotlin
package ch.teko.olten.android.kontaktdatenapp.model

data class Person(val name:String, val alter:Int, val bemerkung:String) {
    
    override fun toString(): String {
        return "$name ($alter)"
    }
    
}
```
+++
### ArrayAdapter mit Person
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val kontakte = ArrayList<Person>()

        kontakte.add(Person("Tom", 30, "ABC"))
        kontakte.add(Person("Alex", 30, "ABC"))
        kontakte.add(Person("Susi", 30, "ABC"))
        kontakte.add(Person("Sabrina", 30, "ABC"))
        kontakte.add(Person("Mario", 30, "ABC"))
        kontakte.add(Person("Luigi", 30, "ABC"))
        kontakte.add(Person("Susanne", 30, "ABC"))
        kontakte.add(Person("Tobi", 30, "ABC"))
        kontakte.add(Person("Gianni", 30, "ABC"))
        kontakte.add(Person("Miriam", 30, "ABC"))
        kontakte.add(Person("Luana", 30, "ABC"))
        kontakte.add(Person("Max", 30, "ABC"))

        val kontakteAdapter = ArrayAdapter<Person>(this, android.R.layout.simple_list_item_1, kontakte)
        kontaktListe.adapter = kontakteAdapter

        kontaktListe.setOnItemClickListener { parent, view, position, id ->
            view as TextView
            Toast.makeText(this, "Geklickt wurde: ${view.text}", Toast.LENGTH_SHORT).show()
        }
    }
}
```
+++
### Eigener Adapter 
#### Layout für ein Record
<img src="http://drive.google.com/uc?export=view&id=1MPm4CHKpVyolJMgRrarxpgV2AskpLLfQ" width="60%"/>
+++
### Layout für ein Record
<img src="http://drive.google.com/uc?export=view&id=1d7TUiXZrnRlPtBF8VMLUgfpjjz3e-2L1" width="70%"/>
+++
### Layout für ein Record
<img src="http://drive.google.com/uc?export=view&id=1esqXlWitEj8Aq_gHkk6q5QvQ7Kjjkopu" width="70%"/>
+++
### Eigener Adapter
```kotlin
class PersonAdapter (context: Context, kontaktDaten:ArrayList<Person>) : BaseAdapter() {

    private val context:Context = context
    private val kontaktDaten = kontaktDaten

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val listenEintrag = layoutInflater.inflate(R.layout.kontakt_layout, parent, false)

        val nameTxt = listenEintrag.findViewById<TextView>(R.id.nameTxt)
        val alterTxt = listenEintrag.findViewById<TextView>(R.id.alterTxt)
        val bemerkungTxt = listenEintrag.findViewById<TextView>(R.id.bemerkungTxt)

        val person = kontaktDaten.get(position)

        nameTxt.text = person.name
        alterTxt.text = person.alter.toString()
        bemerkungTxt.text = person.bemerkung

        return listenEintrag
    }

    override fun getItem(position: Int): Any {
        return kontaktDaten.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return kontaktDaten.size
    }
} 
```
+++
### Activiy anpassen
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val kontakte = ArrayList<Person>()

        kontakte.add(Person("Tom", 30, "ABC"))
        kontakte.add(Person("Alex", 30, "ABC"))
        kontakte.add(Person("Susi", 30, "ABC"))
        kontakte.add(Person("Sabrina", 30, "ABC"))
        kontakte.add(Person("Mario", 30, "ABC"))
        kontakte.add(Person("Luigi", 30, "ABC"))
        kontakte.add(Person("Susanne", 30, "ABC"))
        kontakte.add(Person("Tobi", 30, "ABC"))
        kontakte.add(Person("Gianni", 30, "ABC"))
        kontakte.add(Person("Miriam", 30, "ABC"))
        kontakte.add(Person("Luana", 30, "ABC"))
        kontakte.add(Person("Max", 30, "ABC"))

        val kontakteAdapter = PersonAdapter(this, kontakte)
        kontaktListe.adapter = kontakteAdapter

        kontaktListe.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this, "Geklickt wurde: ${kontakte.get(position)}", Toast.LENGTH_SHORT).show()
        }
    }
}
```
+++
### GIT
[https://bitbucket.org/androidkotlin/kontaktdaten-app/src/listview-ohne-bilder/](https://bitbucket.org/androidkotlin/kontaktdaten-app/src/listview-ohne-bilder/)
+++ 
### Bilder in der ListView
<img src="http://drive.google.com/uc?export=view&id=1EW1L2RMw-mRRVc5iBbdbrkI82wYHkBCG" width="60%"/>
+++
### Minimale Android Version hochsetzen
<img src="http://drive.google.com/uc?export=view&id=1oiUN8NWzWykjV2VHWg7eHTaMTNHFzTJi" width="60%"/>
+++
### Bilder downloaden
[https://drive.google.com/drive/folders/1-8WKHEUup2PYmm2lRyf_rcaOxr3FQlPi?usp=sharing](https://drive.google.com/drive/folders/1-8WKHEUup2PYmm2lRyf_rcaOxr3FQlPi?usp=sharing)
+++
### Bilder importieren
<img src="http://drive.google.com/uc?export=view&id=1LOpkHw50A85J8IxoKGLl6WGOXmdm3mz-" width="60%"/>
+++
### Person erweitern
```kotlin
data class Person(val name:String, val alter:Int, val bemerkung:String, val pic:Drawable) {

    override fun toString(): String {
        return "$name ($alter)"
    }

}
```
+++
### Activity anpassen
```kotlin
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val kontakte = ArrayList<Person>()


        kontakte.add(Person("Susi", 30, "ABC", getDrawable(R.drawable.pers1)))
        kontakte.add(Person("Sabrina", 30, "ABC", getDrawable(R.drawable.pers2)))
        kontakte.add(Person("Luana", 30, "ABC", getDrawable(R.drawable.pers3)))

        val kontakteAdapter = PersonAdapter(this, kontakte)
        kontaktListe.adapter = kontakteAdapter

        kontaktListe.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this, "Geklickt wurde: ${kontakte.get(position)}", Toast.LENGTH_SHORT).show()
        }
    }
}
```
+++
### PersonAdapter anpassen
```kotlin
class PersonAdapter (context: Context, kontaktDaten:ArrayList<Person>) : BaseAdapter() {

    private val context:Context = context
    private val kontaktDaten = kontaktDaten

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val listenEintrag = layoutInflater.inflate(R.layout.kontakt_layout, parent, false)

        val nameTxt = listenEintrag.findViewById<TextView>(R.id.nameTxt)
        val alterTxt = listenEintrag.findViewById<TextView>(R.id.alterTxt)
        val bemerkungTxt = listenEintrag.findViewById<TextView>(R.id.bemerkungTxt)
        val pic = listenEintrag.findViewById<ImageView>(R.id.kontaktImageView)

        val person = kontaktDaten.get(position)

        nameTxt.text = person.name
        alterTxt.text = person.alter.toString()
        bemerkungTxt.text = person.bemerkung
        pic.setImageDrawable(person.pic)

        return listenEintrag
    }

    override fun getItem(position: Int): Any {
        return kontaktDaten.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return kontaktDaten.size
    }
}
```
+++
### Liste mit Bildern
<img src="http://drive.google.com/uc?export=view&id=1n0FaVwhX1cQ519CNFs9SHubsM6yVnooG" width="30%"/>
+++
### GIT
[https://bitbucket.org/androidkotlin/kontaktdaten-app/src/listview-mit-bilder/](https://bitbucket.org/androidkotlin/kontaktdaten-app/src/listview-mit-bilder/)
---
## Kontaktdaten App
### Intents
+++
### Doku
[https://developer.android.com/guide/components/intents-filters](https://developer.android.com/guide/components/intents-filters)
+++
### DetailActiviy erfassen
#### Scrolling Activyy
<img src="http://drive.google.com/uc?export=view&id=1MFIg5mEPbsgkmhV753vwt-Nwag5Qy7j1" width="60%"/>
+++
### Hierarchie festlegen
#### Wichtig für Zurückbutton
<img src="http://drive.google.com/uc?export=view&id=1DVHQS9wZwUN4f6wCZMN6jd86sY-_pMKx" width="60%"/>
+++
### Expliziten Intent erstellen
```kotlin
kontaktListe.setOnItemClickListener { parent, view, position, id ->
    Toast.makeText(this, "Geklickt wurde: ${kontakte.get(position)}", Toast.LENGTH_SHORT).show()
    val detailIntent = Intent(this, DetailActivity::class.java)
    startActivity(detailIntent)
}
```
@[3](IntentObjekt instanzieren)
@[4](Activiy starten)
+++
### Mit Extras Informationen an zweite Activity übergeben
```kotlin
kontaktListe.setOnItemClickListener { parent, view, position, id ->
    Toast.makeText(this, "Geklickt wurde: ${kontakte.get(position)}", Toast.LENGTH_SHORT).show()
    val detailIntent = Intent(this, DetailActivity::class.java)
    detailIntent.putExtra("username", "max")
    startActivity(detailIntent)
}
```
@[4](Informationen für die zweite Activity mit putExtra (Key:Value) hinzufügen)
+++
### Informationen aus Extra entnehmen
```kotlin
class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val username = intent.getStringExtra("username")
        Log.i("TEST", username)
    }
}
```
@[13-14](intent.getStringExtra benutzen um den username aus dem Extra zu holen)
+++
### Kontaktdaten zur DetailActivity senden
```kotlin
kontaktListe.setOnItemClickListener { parent, view, position, id ->
    Toast.makeText(this, "Geklickt wurde: ${kontakte.get(position)}", Toast.LENGTH_SHORT).show()
    val detailIntent = Intent(this, DetailActivity::class.java)
    detailIntent.putExtra("name", kontakte.get(position).name)
    detailIntent.putExtra("alter", kontakte.get(position).alter)
    detailIntent.putExtra("bemerkung", kontakte.get(position).bemerkung)
    startActivity(detailIntent)
}
```
+++
### Kontaktdaten aus Extra entnehmen
```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail)
    setSupportActionBar(toolbar)
    fab.setOnClickListener { view ->
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    val name = intent.getStringExtra("name")
    val alter = intent.getIntExtra("alter", -1)
    val bemerkung = intent.getStringExtra("bemerkung")

    Log.i("TEST", "$name, $alter, $bemerkung")
}
```
@[12](intent.getIntExtra bei einem Integer benutzen)
+++
### Scrolling Activiy anpassen
<img src="http://drive.google.com/uc?export=view&id=10cTIMhZQyqXr2IBas-t1VkJz6JwgkfL8" width="60%"/>
+++
### Scrolling Activiy anpassen
```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail)
    setSupportActionBar(toolbar)
    fab.setOnClickListener { view ->
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    val name = intent.getStringExtra("name")
    val alter = intent.getIntExtra("alter", -1)
    val bemerkung = intent.getStringExtra("bemerkung")

    Log.i("TEST", "$name, $alter, $bemerkung")

    toolbar_layout.title = "$name $alter"
}
```
@[17](Titel setzen)
+++
### Content Seite anpassen
<img src="http://drive.google.com/uc?export=view&id=1s9BT2sTEwX2ywku8ELx_OGzz2IjVqOsJ" width="60%"/>
+++
```kotlin
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail)
    setSupportActionBar(toolbar)
    fab.setOnClickListener { view ->
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    val name = intent.getStringExtra("name")
    val alter = intent.getIntExtra("alter", -1)
    val bemerkung = intent.getStringExtra("bemerkung")

    Log.i("TEST", "$name, $alter, $bemerkung")

    toolbar_layout.title = "$name $alter"
    inhaltTxt.text = bemerkung
}
```
@[18](Bemerkung setzen)
+++
### GIT 
[https://bitbucket.org/androidkotlin/kontaktdaten-app/src/intents/](https://bitbucket.org/androidkotlin/kontaktdaten-app/src/intents/)
+++ 
### onActivityResult
[https://developer.android.com/training/basics/intents/result](https://developer.android.com/training/basics/intents/result) 
--- 
## Aufgabe
+++
### Ziel
![Video](https://www.youtube.com/embed/PYHwHhvGC4I)
+++
### Mögliches Pattern
<img src="http://drive.google.com/uc?export=view&id=1CY0BetLSyxokSub_FWydysSFHylPGcAS" width="80%"/>
---
## Model-View-Controller
+++
### MVC Pattern
[https://medium.com/upday-devs/android-architecture-patterns-part-1-model-view-controller-3baecef5f2b6](https://medium.com/upday-devs/android-architecture-patterns-part-1-model-view-controller-3baecef5f2b6)
+++
### MVC und Repositories
<img src="http://drive.google.com/uc?export=view&id=1B0Y0gvBKdqqZ-NjzQT-WgtYDJ6zl2amT" width="50%"/>
+++
### Beispiel MyBooks App
![Video](https://www.youtube.com/embed/ZMoxi92c3vo)
+++
### MyBooks UML
<img src="http://drive.google.com/uc?export=view&id=118fsGgYGgLwk1csshANK7sV6NlKM5Kb-" width="50%"/>
+++
### GIT
[https://bitbucket.org/androidkotlin/mybooks/src/master/](https://bitbucket.org/androidkotlin/mybooks/src/master/)
---
## Fragments
+++
[https://developer.android.com/guide/components/fragments](https://developer.android.com/guide/components/fragments)
+++
### Demo
![Video](https://www.youtube.com/embed/U5mUoThhiKQ)
+++
### FrameLayout - "Platzhalter" für Fragments
<img src="http://drive.google.com/uc?export=view&id=10TMpXvxCZ64kzMCppdxqou_vL6j_YKkt" width="70%"/>
+++
### Transaction Manager
[Transaktionen](https://de.wikipedia.org/wiki/Transaktion_(Informatik))
```kotlin
class MainActivity : AppCompatActivity() {

    val erstesFragment = ErstesFragment()
    val zweitesFragment = ZweitesFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragment1Btn.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, erstesFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        fragment2Btn.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, zweitesFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

    }
}
```
@[11](Transaktion für erstes Fragment beginnen)
@[12](Frame mit Fragment erstetzen)
@[13](Frame in BackStack setzen - Züruck Button geht auf Frame)
@[14](Transaktion abschliessen)
+++
### ErstesFragment
<img src="http://drive.google.com/uc?export=view&id=1rOyLaauSeSsRqnivQPzdcnQ7e4FpZj8t" width="60%"/>
+++
### Zweites Fragment
<img src="http://drive.google.com/uc?export=view&id=1NTRM3hWzIsMK8CMMqfl2zUjhsllmtMBD" width="60%"/>
+++
### GIT
[https://bitbucket.org/androidkotlin/fragments/src/master/](https://bitbucket.org/androidkotlin/fragments/src/master/)
---
## SQLLite
+++
[https://en.wikipedia.org/wiki/SQLite](https://en.wikipedia.org/wiki/SQLite)
[https://github.com/Kotlin/anko/wiki/Anko-SQLite](https://github.com/Kotlin/anko/wiki/Anko-SQLite)
+++
### Erweiterung MyBooks mit SQLite
<img src="http://drive.google.com/uc?export=view&id=1vLWE8DgrmPV3GTDQGUEwObd4ufw50nij" width="30%"/>

[https://bitbucket.org/androidkotlin/mybooks/src/master/](https://bitbucket.org/androidkotlin/mybooks/src/master/)
+++
### Aktualisierung UI 
<img src="http://drive.google.com/uc?export=view&id=14R67FNH58BcW1zj1wTCUm4mkgIWjuJVD" width="80%"/>
+++
<img src="http://drive.google.com/uc?export=view&id=1aeASbYqp7DQzJIFaJruCbpg2AR60Bq3R" width="80%"/>
+++
### Ankot Dependency hinzufügen
<img src="http://drive.google.com/uc?export=view&id=1htMbOJ2hUgcLXjrKKev3tLY1xEBBL5dv" width="80%"/>
+++
### Application Class
<img src="http://drive.google.com/uc?export=view&id=1E1TJX1CbJv8A-WKXMIlOdzSVkGG14pw8" width="80%"/>
+++
<img src="http://drive.google.com/uc?export=view&id=163i1Fgzf37SwQokBiNWDv5r5Jii8dB1T" width="80%"/>
+++
### DatabaseHelper
<img src="http://drive.google.com/uc?export=view&id=1YEUGgo25WFzOzb8FCKPwhCRO8nqY1D5R" width="80%"/>
+++
### SQLBookRepository
<img src="http://drive.google.com/uc?export=view&id=14QYDYxf7sLQbg7V88wDQJZ-eU1Dxg269" width="80%"/>
+++
### createBook
```kotlin
    override fun createBook(book: Book) {
        databaseHelper.use {
            insert(BookTable.TABLE_NAME,
                    BookTable.TITLE to book.title,
                    BookTable.NOTES to book.notes,
                    BookTable.BORROWED to (if (book.borrowed) 1 else 0))
        }
    }
```
+++
### getAllBooks
```kotlin
    override fun getAllBooks(): List<Book> {
        return databaseHelper.use {
            select(BookTable.TABLE_NAME)
                    .parseList(object : RowParser<Book> {
                        override fun parseRow(columns: Array<Any?>): Book {
                            return parseBook(columns) as Book
                        }
                    })
        }
    }
    
    private fun parseBook(columns: Array<Any?>): Book? {
        val id = columns[0].toString().toInt()
        val title = columns[1].toString()
        val notes = columns[2].toString()
        val borrowed = if (columns[3] == 0L) false else true

        return Book(id, title, notes, borrowed)

    }
```
+++
### findBook
```kotlin
    override fun findBook(id: Int): Book? {
        return databaseHelper.use {
            select(BookTable.TABLE_NAME)
                    .whereSimple("${BookTable.ID} = ?", "$id")
                    .parseOpt(object : RowParser<Book> {
                        override fun parseRow(columns: Array<Any?>): Book {
                            return parseBook(columns) as Book
                        }
                    })
        }
    }

```
+++
### deleteBook
```kotlin
    override fun deleteBook(id: Int) {
            databaseHelper.use {
                delete(BookTable.TABLE_NAME, "_id = {_id}", BookTable.ID to id)
            }
    }
```
+++
### updateBook
```kotlin
    override fun updateBook(changedBook: Book) {
        return databaseHelper.use {
            update(BookTable.TABLE_NAME, BookTable.TITLE to changedBook.title,
                    BookTable.NOTES to changedBook.notes,
                    BookTable.BORROWED to (if (changedBook.borrowed) 1 else 0))
                    .whereSimple("${BookTable.ID} = ?", "${changedBook.id}")
                    .exec()
        }
    }
```
---
## SOLID
[Die SOLID-Prinzipien](http://www.it-designers-gruppe.de/fileadmin/Inhalte/Studentenportal/Die_SOLID-Prinzipien__Folien___1_.pdf)
---
## Unit Test und Mocking
+++
### Was ist ein Unit Test?
* Nicht alles was JUnit verwendet ist auch ein Unit Test.
* Definition von Roy Osherove(«The Art of Unit Testing»)
> «A unit test is an automated piece of code that invokes a unit of work in the system and then checks a single assumption about the behavior of that unit of work.»
+++
### Einstellung zum Testing
* Wir testen um Fehler zu finden. (Eher nicht)
* Wir testen kontinuierlich schon während der Implementation, um von Anfang an Gewissheit zu haben dass es funktioniert. (Das ist der wahre Grund)
+++
### F.I.R.S.T. - Prinzip
* Fast - Tests sollen schnell sein, damit man sie jederzeit und regelmässig ausführt.
* Independent - Tests sollen voneinander unabhängig sein, damit sie in beliebiger Reihenfolge und einzeln ausgeführt werden können.
* Repeatable - Tests sollten in/auf jeder Umgebung lauffähig sein, egal wo und wann.
* Self-Validating - Tests sollen mit einem einfachen boolschen Resultat zeigen ob sie ok sind oder nicht.
* Timely - Tests sollten rechtzeitig, d.h. vor dem produktiven Code geschrieben werden -> 
+++
### Test Bibliotheken
```kotlin
dependencies {
    testCompile 'com.nhaarman.mockitokotlin2:mockito-kotlin:2.0.0-RC1'
    testCompile 'junit:junit:4.12'
    testCompile 'org.mockito:mockito-all:2.0.2-beta'
    compile "org.jetbrains.kotlin:kotlin-stdlib-jdk8"

}
```
+++
### Unit-Test Aufbau
<img src="http://drive.google.com/uc?export=view&id=1ggKOMKSyufplGHUgWOeZ5pvOa6V6rMhV" width="80%"/>
+++
### Problem - Beispiel LendBookManager
<img src="http://drive.google.com/uc?export=view&id=1gq6Ay2dm4h1DbF20-ih1jLn7fYgJCDth" width="80%"/>^
+++
### Mocking
<img src="http://drive.google.com/uc?export=view&id=1SzV7oyczOX2Qs24qn6rF-3dlOAL3R9QD" width="80%"/>
+++
### Lösung mit Mocking
<img src="http://drive.google.com/uc?export=view&id=19vuHt8k_UkreSRlH-u0j7YXDBoij7ciV" width="80%"/>
+++
<img src="http://drive.google.com/uc?export=view&id=1upVERGdBNBJ7uPEJ4i2HzVx6_kxhYEVu" width="80%"/>
+++
### GIT
[https://bitbucket.org/androidkotlin/banking-with-mocks/src/master/](https://bitbucket.org/androidkotlin/banking-with-mocks/src/master/)
---
## RESTful API mit Kotlin
+++
### RESTful Services
[Introduction to Rest API's](https://www.slideshare.net/patricksavalle/rest-cursus)
+++
### Spring Boot
<img src="http://drive.google.com/uc?export=view&id=1xSQ1bG98Li3j6POUVZo4zxaVH9DGRwfC" width="70%"/>
+++
### Rest API with Spring Boot
[Creating a Rest API Effortlessly](https://developersoapbox.com/creating-a-rest-api-effortlessly-with-spring-rest-repositories-kotlin-edition/)
+++
### Mapping URL zu Funktion mit RestController
[RestController](http://kotlination.com/spring/build-springboot-kotlin-restful-web-service)
+++
### book-api Beispiel
+++
### Todo Android App
<img src="http://drive.google.com/uc?export=view&id=1FfktGqrDuvE2uWYUbkEpUDJj_4C-NSMl" width="70%"/>
+++
### POST: todos -> createTodo
<img src="http://drive.google.com/uc?export=view&id=1-gnUIUDXdMdrgO6AfKJff-qXnIe4uYuy" width="70%"/>
+++
### Let's Code
<img src="http://drive.google.com/uc?export=view&id=1pdWfJ1t4FHSTNqQ2rC8_3vpBC31FR6hn" width="80%"/>
+++
### GET: todos -> getAllTodos
<img src="http://drive.google.com/uc?export=view&id=1mz_ZT59OFYixv2Ls_UOumjIuRIgzjTH-" width="80%"/>
+++
<img src="http://drive.google.com/uc?export=view&id=1kZILz--HMr3irATbv38TaRUzOyFUvoHt" width="30%"/>
+++
### Let's Code
<img src="http://drive.google.com/uc?export=view&id=18mlAG4ag_5z5cN2eOC5hxaTL59r4UU3r" width="80%"/>
+++
### GET: todos/2 -> getTodo
<img src="http://drive.google.com/uc?export=view&id=1GWLLf4OIJqXho2ic5-tb35b7X9ii6SNm" width="70%"/>
+++
### Let's Code
<img src="http://drive.google.com/uc?export=view&id=1ZZDg7RmfXjO10RK0ZGf0so24fj0Gaysy" width="80%"/>
+++
### PUT: todos/2 -> updateTodoById
### Let's Code
<img src="http://drive.google.com/uc?export=view&id=19i1ZMNpvZ-_QAnEdHsFsrrCCbGlWJh6-" width="80%"/>
+++
### DELETE: todos/2 -> deleteTodoById
### Let's Code
<img src="http://drive.google.com/uc?export=view&id=1Qlm2EtPduiBds1qHUpZM2Rx8SPFXwbUM" width="80%"/>
---
### REST API im Android Client anbinden
